##Informational Retrival Programming Assignment 1

This Project is done to understand Lucene Library

Java version - jdk 8 

** Libraries used and their versions **

    1. lucene-core:7.6.0 
   	2. jsoup: '1.11.3'
    3. lucene-queryparser:7.5.+'
    4. lucene-analyzers-common'7.5.0'
    
** Functionalities of libraries **
	
	
	Lucene_queryparser - 7.5.0 used for search the indexes retreival 
	Lucene_analyzers - 7.5.0 english lang analyzer stemmer
	Lucene_core - 7.6.0 has func related to indexing docs
	jsoup - parsing html files

i) Lucene_core
	This library is used for the functionality of indexing the documents.
	
ii) Lucene_jsoup 
	This library is used for parsing the HTML files.
	
iii) Lucene_analyzers 
	This library is used for preprocessing which includes the functionalities of stemming the words, stopword removal,tokenization and lower case conversion of search text . 	
	
iv) Lucene_queryparser 
	This library is used for creating parsed queries based on the given query text.



## Setting up project

** Prerequisites **
 
1. jdk 8 or above.
2. Eclipse or Spring tool suite(STS) IDE with Buildship gradle plugin installed. If not download STS from the link https://spring.io/tools.
3. STS/ECLIPSE workspace is setup.

---
** Steps / User Guide **

1. File -> import ->gradle project.


## TO RUN 

java -jar IR P.jar [path to document folder] [path to index folder] [VS/OK] [query]

Example : java -jar IR_P.jar "D:\courses\IR\Documents\Documents" "D:\courses\IR\Documents\indexFolder" "VS" "SearchText"



