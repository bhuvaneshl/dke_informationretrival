/**
 * 
 */
package com.ir;

import java.io.IOException;

import org.apache.lucene.queryparser.classic.ParseException;

/**
 * Main class
 *
 * @author Bhuvanesh, Saijal, Aarthi
 */
public class LuceneAssignment {

	/**
	 * The main method.
	 *
	 * @param args- command line arguments
	 * @throws IOException    Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 */
	public static void main(String[] args) throws IOException, ParseException {
		if (args.length != 4) {
			System.out.println("Requried Parameters not passed");
			System.out.println("Format");
			System.out.println("java -jar IR P.jar [path to document folder] [path to index folder] [VS/OK] [query]");
		} else {
			// argument order [document directory] [index directory] [Ranking Model] [search
			// text/Query]
			String documentsDirectory = args[0];
			String indexDirectory = args[1];
			String rankingModel = args[2];
			String searchText = args[3];
			IndexAndSearch indexAndSearch = new IndexAndSearch(documentsDirectory, indexDirectory);
			indexAndSearch.indexDocuments();
			indexAndSearch.searchDocuments(searchText, rankingModel);
		}
	}

}
