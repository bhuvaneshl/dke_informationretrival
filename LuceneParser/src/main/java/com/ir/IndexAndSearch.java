package com.ir;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.jsoup.Jsoup;


/**
 * <B>The Class IndexAndSearch</B><br>.
 *  Handles indexing of non indexed documents in the given document directory <br>
 *  Handles searching and ranking documents for the given search string
 *  
 */
public class IndexAndSearch {
	
	/** The index directory. */
	private Directory indexDirectory;
	
	/** The document folder path. */
	private Path documentFolderPath;
	
	/** The index folder path. */
	private Path indexFolderPath;
	
	/** Index a file Functional Interface method. */
	private BiConsumer<DocIndexStructure, IndexWriter> indexAFile = (DocIndexStructure docIndexStructure,
			IndexWriter indexWriter) -> {
		Document document = new Document();
		System.out.println("indexing " + docIndexStructure.path);

		document.add(new StringField(DocumentConstants.TAG_DOCUMENT_PATH, docIndexStructure.path, Field.Store.YES));
		document.add(new StringField(DocumentConstants.TAG_DOCUMENT_TITLE, docIndexStructure.title.toLowerCase(), Field.Store.YES));
		document.add(new TextField(DocumentConstants.TAG_DOCUMENT_BODY, docIndexStructure.body, Field.Store.YES));
		try {
			indexWriter.addDocument(document);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};

	/**
	 * check whether given file path ends with .html,.htm or .txt
	 */
	private Predicate<Path> isTextOrHtmlFile = path -> {
		String fileName = path.toFile().toString().toLowerCase();
		if (fileName.endsWith(DocumentConstants.DOCUMENT_TYPE_TXT)
				|| fileName.endsWith(DocumentConstants.DOCUMENT_TYPE_HTML)
				|| fileName.endsWith(DocumentConstants.DOCUMENT_TYPE_HTM)) {
			return true;
		}
		return false;
	};

	/**
	 * Gets the document contents as string.
	 *
	 * @param p the path of the file
	 * @return the document contents as string
	 */
	String getDocumentContentsAsString(Path p) {

		StringBuilder stringBuilder = new StringBuilder();
		try {
			Files.readAllLines(p,StandardCharsets.UTF_8).forEach(line -> stringBuilder.append(line).append("\n"));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		return stringBuilder.toString();
	}

	/**
	 * constructor.
	 *
	 * @param documentFolder the document folder
	 * @param indexFolder the index folder
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public IndexAndSearch(String documentFolder, String indexFolder) throws IOException {
		documentFolderPath = Paths.get(documentFolder);
		indexFolderPath = Paths.get(indexFolder);
		indexDirectory = FSDirectory.open(indexFolderPath);

	}

	/**
	 * get document content , title and path, for html documents it strips tags and
	 * extracts only the contents.
	 *
	 * @param path the path
	 * @return the index data
	 */
	private DocIndexStructure getIndexData(Path path) {
		DocIndexStructure indexStructure = new DocIndexStructure();
		indexStructure.path = path.toString();
		// if html document parse and retrieve title and content
		if (path.getFileName().toString().toLowerCase().endsWith(DocumentConstants.DOCUMENT_TYPE_HTML)
				|| path.getFileName().toString().toLowerCase().endsWith(DocumentConstants.DOCUMENT_TYPE_HTM)) {
			org.jsoup.nodes.Document parsedHtmlDoc = Jsoup.parse(getDocumentContentsAsString(path));
			if (parsedHtmlDoc.title() == null || parsedHtmlDoc.title().isEmpty()) {
				indexStructure.title = path.getFileName().toString();
			} else {
				indexStructure.title = parsedHtmlDoc.title();
			}
			indexStructure.body = parsedHtmlDoc.body().text();
		} else { // text document
			indexStructure.title = path.getFileName().toString();
			indexStructure.body = getDocumentContentsAsString(path);
		}

		return indexStructure;
	}

	/**
	 * Class to hold Index Document field data.
	 */
	private final class DocIndexStructure {
		
		/** The title. */
		String title;
		
		/** The path. */
		String path;
		
		/** The body. */
		String body;
	}

	/**
	 * Index documents.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void indexDocuments() throws IOException {
		// English Analyzer is used as stemmer for English language
		IndexWriter indexWriter = new IndexWriter(indexDirectory, new IndexWriterConfig(new EnglishAnalyzer()));
		// this commit is done to avoid index not found exception thrown when index
		// directory is empty
		indexWriter.commit();
		IndexReader indexReader = DirectoryReader.open(indexDirectory);
		IndexSearcher indexSearcher = new IndexSearcher(indexReader);
		// files.walk will scan for files in a given folder and all inner folders ,
		// first filter is used select only text and html files
		Files.walk(documentFolderPath).filter(isTextOrHtmlFile).forEach(path -> {
			Term term = new Term(DocumentConstants.TAG_DOCUMENT_PATH, path.toString());
			try {
				// check whether document is indexed already
				if (indexSearcher.search(new TermQuery(term), 1).totalHits == 0) {
					indexAFile.accept(getIndexData(path), indexWriter);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		// close index writer as indexing is complete
		indexWriter.close();
	}

	/**
	 * Search documents.
	 *
	 * @param searchText the search text
	 * @param rankingModel the ranking model
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 */
	void searchDocuments(String searchText, String rankingModel) throws IOException, ParseException {
		IndexReader indexReader = DirectoryReader.open(indexDirectory);
		IndexSearcher indexSearcher = new IndexSearcher(indexReader);
		// only top 10 results needs to be displayed
		int numberOfResults = 10;
		MultiFieldQueryParser multiFieldParser = new MultiFieldQueryParser(
				new String[] { DocumentConstants.TAG_DOCUMENT_TITLE, DocumentConstants.TAG_DOCUMENT_BODY },
				new StandardAnalyzer());
		Query query = multiFieldParser.parse(searchText);
		if (DocumentConstants.RANKING_MODEL_VECTOR_SPACE.equalsIgnoreCase(rankingModel)) {
			indexSearcher.setSimilarity(new ClassicSimilarity());
		} else if (DocumentConstants.RANKING_MODEL_OKAPI_BM25.equalsIgnoreCase(rankingModel)) {
			indexSearcher.setSimilarity(new BM25Similarity());
		} else {
			System.out.println("Invalid Ranking Model passed as input");
			return;
		}
		TopDocs matchedDocs = indexSearcher.search(query, numberOfResults);
		Document currentLuceneDoc;
		int rank = 1;
		if (matchedDocs.scoreDocs.length > 0) {
			// Rank -7 chars Relavance Score -19 chars File Name/Title - 29 chars
			System.out.println("Rank   Relavance Score    File Name/Title              Path ");
			for (ScoreDoc scoreDoc : matchedDocs.scoreDocs) {
				// get lucene document
				currentLuceneDoc = indexSearcher.doc(scoreDoc.doc);
				System.out.println(String.format("%-7d%-19.3f%-29s%s", rank++, scoreDoc.score,
						currentLuceneDoc.get(DocumentConstants.TAG_DOCUMENT_TITLE),
						currentLuceneDoc.get(DocumentConstants.TAG_DOCUMENT_PATH)));
			}
		} else {
			System.out.println("No Match Found for given query");
		}

	}

}
