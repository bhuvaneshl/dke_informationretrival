/**
 * Constants file
 */
package com.ir;

// TODO: Auto-generated Javadoc
/**
 *  Class DocumentConstants.
 *
 * @author Bhuvanesh, Saijal , Aarthi
 */
public class DocumentConstants {

	/** The Constant DOCUMENT_TYPE_TXT. */
	public static final String DOCUMENT_TYPE_TXT = ".txt";

	/** The Constant DOCUMENT_TYPE_HTML. */
	public static final String DOCUMENT_TYPE_HTML = ".html";

	/** The Constant DOCUMENT_TYPE_HTM. */
	public static final String DOCUMENT_TYPE_HTM = ".htm";

	/** The Constant TAG_DOCUMENT_BODY. */
	public static final String TAG_DOCUMENT_BODY = "docBody";

	/** The Constant TAG_DOCUMENT_PATH. */
	public static final String TAG_DOCUMENT_PATH = "docPath";

	/** The Constant TAG_DOCUMENT_TITLE. */
	public static final String TAG_DOCUMENT_TITLE = "docTitle";
	
	/** The Constant RANKING_MODEL_VECTOR_SPACE. */
	public static final String RANKING_MODEL_VECTOR_SPACE = "VS";
	
	/** The Constant RANKING_MODEL_OKAPI_BM25. */
	public static final String RANKING_MODEL_OKAPI_BM25 = "OK";
	
}
